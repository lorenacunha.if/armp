import Home from "./pages/home/home";
import "./styles/common.css";

function App() {
  return <Home></Home>;
}

export default App;
