import { Layout } from "antd";
import { useEffect, useState } from "react";
import AppContent from "../../components/content/content";
import AppHeader from "../../components/header/header";
import AppNavbar from "../../components/navbar/navbar";
import { levels } from "../../mockup/levels";
import "./home.css";
import TreeStore from "../../stores/tree-store";

const Home = () => {
  const [breadcrumbs, setBreadcrumbs] = useState<(string | undefined)[]>([]);
  const [levelsList, setLevelsList] = useState(levels);
  const [currentLevel, setCurrentLevel] = useState(1);

  const updateBreadcrumbs = () => {
    const bc = levelsList
      .filter((level) => level.level <= currentLevel)
      .map((level) => level.label);
    setBreadcrumbs(bc);
  };

  useEffect(() => {
    updateBreadcrumbs();
  }, [currentLevel]);

  return (
    <TreeStore>
      <Layout className='fullscreen-container'>
        <AppHeader></AppHeader>
        <div className='main'>
          <AppNavbar breadcrumbs={breadcrumbs}></AppNavbar>
          <AppContent></AppContent>
        </div>
      </Layout>
    </TreeStore>
  );
};
export default Home;
