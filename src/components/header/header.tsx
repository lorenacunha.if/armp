import { Header } from "antd/es/layout/layout";
import Logo from "../../assets/imgs/logo.svg";
import "./header.css";

const AppHeader = () => {
  return (
    <Header className='header'>
      <img className='brand-logo' src={Logo} alt='logo' />
      <span className='brand-title'>Equip Branch Mapping</span>
    </Header>
  );
};
export default AppHeader;
