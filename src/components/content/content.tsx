import Search, { SearchProps } from "antd/es/input/Search";
import { Content } from "antd/es/layout/layout";
import Tree, { DataNode } from "antd/es/tree";
import { useContext, useEffect, useMemo, useState } from "react";
import { TreeContext } from "../../stores/tree-store";
import SiteOptions from "../options/site-options/site-options";
import "./content.css";
import DepartmentOptions from "../options/departament-options/department-options";

type propsType = {};

const AppContent = (props: propsType) => {
  const { showTree, tree, currentLevel, search, setSearch, expandedKeys, setExpandedKeys } =
    useContext(TreeContext);

  const [searchValue, setSearchValue] = useState("");
  const [autoExpandParent, setAutoExpandParent] = useState(true);
  const defaultData: DataNode[] = [];

  const onExpand = (newExpandedKeys: React.Key[]) => {
    setExpandedKeys(newExpandedKeys);
    setAutoExpandParent(false);
  };

  const treeData = useMemo(() => {
    const loop = (data: DataNode[]): DataNode[] =>
      data.map((item) => {
        const strTitle = item.title as string;
        const index = strTitle.indexOf(searchValue);
        const beforeStr = strTitle.substring(0, index);
        const afterStr = strTitle.slice(index + searchValue.length);
        const title =
          index > -1 ? (
            <span>
              {beforeStr}
              <span className='site-tree-search-value'>{searchValue}</span>
              {afterStr}
            </span>
          ) : (
            <span>{strTitle}</span>
          );
        if (item.children) {
          return { title, key: item.key, children: loop(item.children) };
        }

        return {
          title,
          key: item.key,
        };
      });

    return loop(defaultData);
  }, [searchValue]);

  const getParentKey = (key: React.Key, tree: DataNode[]): React.Key => {
    let parentKey: React.Key;
    for (let i = 0; i < tree.length; i++) {
      const node = tree[i];
      if (node.children) {
        if (node.children.some((item) => item.key === key)) {
          parentKey = node.key;
        } else if (getParentKey(key, node.children)) {
          parentKey = getParentKey(key, node.children);
        }
      }
    }
    return parentKey!;
  };

  const onSearch: SearchProps["onSearch"] = (value: any, _e: any, info: any) => {
    console.log(info?.source, value);
    setSearch(value);
  };

  const onChange = (elem: any) => {
    setSearch(elem.target.value);
  };

  const updateTreeElement = () => {
    return <Tree showLine showIcon treeData={tree} expandedKeys={expandedKeys} />;
  };

  useEffect(() => {
    console.log(`updateTreeElement`, { tree });
    updateTreeElement();
  }, [tree, expandedKeys]);

  return (
    <Content className={showTree ? "justify-content-start" : "justify-content-center"}>
      <div>
        {showTree ? (
          <div className='tree-content'>
            <Search placeholder='input search text' allowClear onSearch={onSearch} />
            {updateTreeElement()}
            <Tree
              onExpand={onExpand}
              expandedKeys={expandedKeys}
              autoExpandParent={autoExpandParent}
              treeData={treeData}
            />
          </div>
        ) : (
          ""
        )}
      </div>
      <div className='main-content'>
        <Search placeholder='input search text' allowClear value={search} onChange={onChange} />
        {currentLevel == 1 ? <SiteOptions></SiteOptions> : <DepartmentOptions></DepartmentOptions>}
      </div>
      <div className='consult-content'></div>
    </Content>
  );
};
export default AppContent;
