import { Breadcrumb } from "antd";
import "./navbar.css";

type propsType = {
  breadcrumbs: (string | undefined)[];
};

const AppNavbar = (props: propsType) => {
  return (
    <Breadcrumb>
      {props.breadcrumbs.map((bread, index) => (
        <Breadcrumb.Item key={`${bread}-${index}`}>{bread}</Breadcrumb.Item>
      ))}
    </Breadcrumb>
  );
};
export default AppNavbar;
