import { PlusOutlined } from "@ant-design/icons";
import { Badge, Button, List } from "antd";
import { useEffect, useState, useContext } from "react";
import { departments } from "../../../mockup/departments";
import "./department-options.css";
import { TreeContext } from "../../../stores/tree-store";

const DepartmentOptions = (props: {}) => {
  const { search, addToTree } = useContext(TreeContext);
  const [items, setItems] = useState<any[]>(departments.filter((dep: any) => dep.label));
  const [itemsFiltered, setItemsFiltered] = useState<any[]>([]);

  const applyItemsSearch = (value: string) => {
    setItemsFiltered(
      items.filter((item) => item.label.toLowerCase().includes(value?.toLowerCase())),
    );
  };

  useEffect(() => {
    applyItemsSearch(search);
  }, [search]);

  const getItemStatus = (
    status: any,
  ): "default" | "success" | "processing" | "error" | "warning" | undefined => {
    if (!status) return "default";
    if (status == "progress") return "processing";
    if (status == "done") return "success";
    return "default";
  };

  return (
    <List>
      {itemsFiltered?.map((item, index) => (
        <List.Item key={`${item.label} ${index}`}>
          <Badge status={getItemStatus(item.status)} />
          <List.Item.Meta title={item.label} description={`${item.abbrev}`} />
          <Button
            shape='circle'
            icon={<PlusOutlined />}
            size='middle'
            onClick={() => addToTree(item)}
          />
        </List.Item>
      ))}
    </List>
  );
};
export default DepartmentOptions;
