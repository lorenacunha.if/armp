import { Badge, List } from "antd";
import { useContext, useEffect, useState } from "react";
import { TreeContext } from "../../../stores/tree-store";
import "./site-options.css";
import { sites } from "../../../mockup/sites";

const SiteOptions = (props: {}) => {
  const { search, initTree } = useContext(TreeContext);
  const [items, setItems] = useState<any[]>(sites.filter((site) => site.label && site.abbrev));
  const [itemsFiltered, setItemsFiltered] = useState<any[]>([]);

  const applyItemsSearch = (value: string) => {
    setItemsFiltered(
      items.filter((item) => item.label.toLowerCase().includes(value?.toLowerCase())),
    );
  };

  useEffect(() => {
    applyItemsSearch(search);
  }, [search]);

  const getItemStatus = (
    status: any,
  ): "default" | "success" | "processing" | "error" | "warning" | undefined => {
    if (!status) return "default";
    if (status == "progress") return "processing";
    if (status == "done") return "success";
    return "default";
  };

  return (
    <List>
      {itemsFiltered?.map((item, index) => (
        <List.Item
          key={`${item.label} ${index}`}
          className='options'
          onClick={() => initTree(item)}
        >
          <Badge status={getItemStatus(item.status)} />
          <List.Item.Meta
            title={item.label}
            description={`${item.code}${item.code ? ": " : ""}${item.abbrev}${
              item.abbrev ? "/" : ""
            }${item.pm_system}`}
          />
        </List.Item>
      ))}
    </List>
  );
};
export default SiteOptions;
