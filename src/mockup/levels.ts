export const levels = [
  {
    level: 1,
    label: "site",
  },
  {
    level: 2,
    label: "department",
  },
  {
    level: 3,
    label: "installation",
  },
  {
    level: 4,
    label: "areas / services",
  },
  {
    level: 5,
    label: "machines",
  },
  {
    level: 6,
    label: "machines subset",
  },
  {
    level: 7,
    label: "part from machine subset",
  },
  {
    level: 8,
    label: "",
  },
  {
    level: 9,
    label: "",
  },
  {
    level: 10,
    label: "",
  },
  {
    level: 11,
    label: "",
  },
  {
    level: 12,
    label: "",
  },
  {
    level: 13,
    label: "",
  },
];
