export const installation = [
  {
    Department: "MO",
    Installation: "RAW1",
    label: "Coal-Mine Operations",
  },
  {
    Department: "MO",
    Installation: "RAW2",
    label: "Iron-Mine Operations",
  },
  {
    Department: "MO",
    Installation: "MPE",
    label: "Common Mine Production Equipment",
  },
  {
    Department: "MO",
    Installation: "MAE",
    label: "Common Mine Auxiliary Equipment",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "CT",
    Installation: "CON",
    label: "Concentrating Plant Line",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "CP",
    Installation: "FCR",
    label: "Fixed Crushing Plant Line",
  },
  {
    Department: "CP",
    Installation: "MCR",
    label: "Mobile Crushing Plant line",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "DR",
    Installation: "DRI",
    label: "DRI Production line N°1 (Process)",
  },
  {
    Department: "DR",
    Installation: "RAW2",
    label: "Direct Reduced Iron (Bullet Plant)",
  },
  {
    Department: "DR",
    Installation: "COM",
    label: "Common Services",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "CB",
    Installation: "COP",
    label: "Coal Preparation",
  },
  {
    Department: "CB",
    Installation: "COB",
    label: "Coke Battery",
  },
  {
    Department: "CB",
    Installation: "GAT",
    label: "Gas Treatment/By-products",
  },
  {
    Department: "CB",
    Installation: "SCP",
    label: "Screening Plant",
  },
  {
    Department: "CB",
    Installation: "STE",
    label: "Steelanolplant",
  },
  {
    Department: "CB",
    Installation: "COM",
    label: "Common Services for Coke Plant",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "SB",
    Installation: "RMP",
    label: "Raw material preparation for sinterplants & blast furnace(incl industrial dedusting)",
  },
  {
    Department: "SB",
    Installation: "FSI",
    label: "Feeding for sinterplant",
  },
  {
    Department: "SB",
    Installation: "FCO",
    label: "Feeding for sinterplant&blast furnace",
  },
  {
    Department: "SB",
    Installation: "SIN",
    label: "Sinter Plant",
  },
  {
    Department: "SB",
    Installation: "BLF",
    label: "Blast Furnace",
  },
  {
    Department: "SB",
    Installation: "COM",
    label: "Common - Sinter Plant & Blast Furnace",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "ST",
    Installation: "PCC",
    label: "Preparation of convertor charge",
  },
  {
    Department: "ST",
    Installation: "BOF",
    label: "Basic Oxigen Furnace Area",
  },
  {
    Department: "ST",
    Installation: "AOD",
    label: "Argon Oxygen Decarburization",
  },
  {
    Department: "ST",
    Installation: "OHF",
    label: "Open hearth furnace",
  },
  {
    Department: "ST",
    Installation: "EAF",
    label: "Electric Arc Furnace",
  },
  {
    Department: "ST",
    Installation: "LAF",
    label: "Ladle Furnace",
  },
  {
    Department: "ST",
    Installation: "SLM",
    label: "Secondary Ladle Metallurgy",
  },
  {
    Department: "ST",
    Installation: "CCS",
    label: "Continuous Caster Slabs",
  },
  {
    Department: "ST",
    Installation: "CCB",
    label: "Continuous Caster Blooms and Billets ",
  },
  {
    Department: "ST",
    Installation: "INC",
    label: "Ingot Casting",
  },
  {
    Department: "ST",
    Installation: "CCR",
    label: "Continuous Caster Rotating",
  },
  {
    Department: "ST",
    Installation: "VAR",
    label: "Common Vacuum Arc Remelting ",
  },
  {
    Department: "ST",
    Installation: "ESR",
    label: "Common Electro Slag Remelting",
  },
  {
    Department: "ST",
    Installation: "VIM",
    label: "Common Vacuum Induction Melting",
  },
  {
    Department: "ST",
    Installation: "IMF",
    label: "Common Induction Melting Furnace ",
  },
  {
    Department: "ST",
    Installation: "COM",
    label: "Common Services for Steel Plant",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "SY",
    Installation: "BLM",
    label: "Blooming Mill",
  },
  {
    Department: "SY",
    Installation: "SLB",
    label: "Slabbing mill",
  },
  {
    Department: "SY",
    Installation: "SLY",
    label: "Slab, Bloom or Billet Yard",
  },
  {
    Department: "SY",
    Installation: "COM",
    label: "Common - Slab, Bloom or Billet Yard",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "HR",
    Installation: "HRM",
    label: "Hot Rolling Mill",
  },
  {
    Department: "HR",
    Installation: "HPM",
    label: "Heavy plate mill",
  },
  {
    Department: "HR",
    Installation: "HTF",
    label: "Tunnel Furnace",
  },
  {
    Department: "HR",
    Installation: "RCD",
    label: "Reconditioning Line ",
  },
  {
    Department: "HR",
    Installation: "COM",
    label: "Common Services for Hot Rolling Mill",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "LR",
    Installation: "BAR",
    label: "Bar mill",
  },
  {
    Department: "LR",
    Installation: "SCM",
    label: "Section mill",
  },
  {
    Department: "LR",
    Installation: "BRM",
    label: "Billets Rolling mill",
  },
  {
    Department: "LR",
    Installation: "WRM",
    label: "Wire Rod Mill",
  },
  {
    Department: "LR",
    Installation: "BWR",
    label: "SCMs and wire rod mill",
  },
  {
    Department: "LR",
    Installation: "HSM",
    label: "Heavy Sections Mill (or Rail mill)",
  },
  {
    Department: "LR",
    Installation: "MSM",
    label: "Medium Sections Mill(SCMs<35,5kg/m)",
  },
  {
    Department: "LR",
    Installation: "LSM",
    label: "Light Sections Mill (SCMs<3kg/m)",
  },
  {
    Department: "LR",
    Installation: "PCW",
    label: "Pickling Wire Rod",
  },
  {
    Department: "LR",
    Installation: "BAT",
    label: "Batch annealing thermal furnace",
  },
  {
    Department: "LR",
    Installation: "COM",
    label: "Common Services for Long Rolling Mill",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "LW",
    Installation: "BUM?",
    label: "Building meshes line",
  },
  {
    Department: "LW",
    Installation: "WDR?",
    label: "Wire drawing line",
  },
  {
    Department: "LW",
    Installation: "WEL?",
    label: "Welding wire department",
  },
  {
    Department: "LW",
    Installation: "PWR?",
    label: "Profile wire rod mill",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "CR",
    Installation: "PCL",
    label: "Pickling Line",
  },
  {
    Department: "CR",
    Installation: "CRM",
    label: "Full Hard Cold Rolling Mill",
  },
  {
    Department: "CR",
    Installation: "PTM",
    label: "Full Hard Coupled Pickling-Tandem Mill ",
  },
  {
    Department: "CR",
    Installation: "BAF",
    label: "Batch Annealing Furnaces",
  },
  {
    Department: "CR",
    Installation: "CLL",
    label: "Full Hard Cleaning Line",
  },
  {
    Department: "CR",
    Installation: "SKP",
    label: "Skin Pass (Temper)",
  },
  {
    Department: "CR",
    Installation: "CAL",
    label: "Continuous Annealing Line",
  },
  {
    Department: "CR",
    Installation: "COM",
    label: "Common Services for Cold Rolling Mill",
  },
  {
    Department: "CR",
    Installation: "APL",
    label: "Annealing and Pickling Line",
  },
  {
    Department: "CR",
    Installation: "BAL",
    label: "Brighting annealing line",
  },
  {
    Department: "CR",
    Installation: "TFC",
    label: "Treatment Final Carlite",
  },
  {
    Department: "CR",
    Installation: "CGM",
    label: "Coils Grinder Machine ",
  },
  {
    Department: "CR",
    Installation: "CPL",
    label: "Coil Preparation Line",
  },
  {
    Department: "CR",
    Installation: "SKL",
    label: "Skin Pass&Tension Leveling",
  },
  {
    Department: "CR",
    Installation: "COM",
    label: "Common Services - Cold Rolling Mill",
  },
  {
    Department: "CR",
    Installation: "WKSR",
    label: "Roll Shop Workshop",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "RP",
    Installation: "INL",
    label: "Inspection Line/Recoiling ",
  },
  {
    Department: "RP",
    Installation: "SLT",
    label: "Slitting Line",
  },
  {
    Department: "RP",
    Installation: "SLT",
    label: "Slitting Line for complex double lines",
  },
  {
    Department: "RP",
    Installation: "CTL",
    label: "Cut to Length - Shearing",
  },
  {
    Department: "RP",
    Installation: "PFL",
    label: "Profiling Line",
  },
  {
    Department: "RP",
    Installation: "TRA",
    label: "Traversing",
  },
  {
    Department: "RP",
    Installation: "PAL",
    label: "Packing Line Rolls",
  },
  {
    Department: "RP",
    Installation: "PAP",
    label: "Packing line Plates",
  },
  {
    Department: "RP",
    Installation: "CPO",
    label: "Coil Polishing",
  },
  {
    Department: "RP",
    Installation: "SBL",
    label: "Stretch-bend-levelling line",
  },
  {
    Department: "RP",
    Installation: "COM",
    label: "Common services",
  },
  {
    Department: "RP",
    Installation: "PTF",
    label: "Plate Treatment Furnace",
  },
  {
    Department: "RP",
    Installation: "PCP",
    label: "Plate Chemistry Pickling",
  },
  {
    Department: "RP",
    Installation: "PLM",
    label: "Plasma Machine",
  },
  {
    Department: "RP",
    Installation: "CTD",
    label: "Cut to disk",
  },
  {
    Department: "RP",
    Installation: "PWR",
    label: "Peeling Wire Rod",
  },
  {
    Department: "RP",
    Installation: "SIT",
    label: "Steel Invariable Temperature ",
  },
  {
    Department: "RP",
    Installation: "ALY",
    label: " ALLOY",
  },
  {
    Department: "RP",
    Installation: "MTC",
    label: "Multicoil Line",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "CO",
    Installation: "HDG",
    label: "Hot Dip Galvanizing",
  },
  {
    Department: "CO",
    Installation: "EGL",
    label: "Electro Galvanizing",
  },
  {
    Department: "CO",
    Installation: "OCO",
    label: "Organic Coating Line",
  },
  {
    Department: "CO",
    Installation: "CAH",
    label: "Combined annealing and hot dip galvanizing line",
  },
  {
    Department: "CO",
    Installation: "OCP",
    label: "Organic Coating Line Plates",
  },
  {
    Department: "CO",
    Installation: "TIL",
    label: "Tin and Chromium Coating Line",
  },
  {
    Department: "CO",
    Installation: "VDL",
    label: "Vacuum deposit line",
  },
  {
    Department: "CO",
    Installation: "CGO",
    label: "Coupled Galva Organic",
  },
  {
    Department: "CO",
    Installation: "COM",
    label: "Common Services",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "PS",
    Installation: "PTI",
    label: "Plate treatment installations",
  },
  {
    Department: "PS",
    Installation: "COM",
    label: "Common Services",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "TB",
    Installation: "BLK",
    label: "Blanking Line",
  },
  {
    Department: "TB",
    Installation: "LWS",
    label: "Laser Welding Street",
  },
  {
    Department: "TB",
    Installation: "ABL",
    label: "Ablation Line",
  },
  {
    Department: "TB",
    Installation: "COM",
    label: "Common services",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "SP",
    Installation: "EFD",
    label: "Energy&fluids Distribution/Supply",
  },
  {
    Department: "SP",
    Installation: "QAL",
    label: "Quality Management",
  },
  {
    Department: "SP",
    Installation: "GEN",
    label: "General Services",
  },
  {
    Department: "SP",
    Installation: "EHS",
    label: "Environment, health & Safety ",
  },
  {
    Department: "SP",
    Installation: "ENV",
    label: "Environment",
  },
  {
    Department: "SP",
    Installation: "ACC",
    label: "Accounting",
  },
  {
    Department: "SP",
    Installation: "ENG",
    label: "Engineering",
  },
  {
    Department: "SP",
    Installation: "HRM",
    label: "Human resources",
  },
  {
    Department: "SP",
    Installation: "PUR",
    label: "Purchasing",
  },
  {
    Department: "SP",
    Installation: "ITS",
    label: "Information Technology and Services",
  },
  {
    Department: "SP",
    Installation: "IEC",
    label: "Internal and external communication",
  },
  {
    Department: "SP",
    Installation: "EDU",
    label: "Education",
  },
  {
    Department: "SP",
    Installation: "CRM",
    label: "Customer relation management",
  },
  {
    Department: "SP",
    Installation: "HAR",
    label: "Harbor",
  },
  {
    Department: "SP",
    Installation: "RAW",
    label: "Raw Materials",
  },
  {
    Department: "SP",
    Installation: "ITR",
    label: "Internal Transport",
  },
  {
    Department: "SP",
    Installation: "RCP",
    label: "Recuperation and by-products",
  },
  {
    Department: "SP",
    Installation: "SOC",
    label: "Social department",
  },
  {
    Department: "SP",
    Installation: "COM",
    label: "Common Services Production",
  },
  {
    Department: "SP",
    Installation: "WKS",
    label: "Workshops",
  },
  {
    Department: "SP",
    Installation: "WKSR",
    label: "Roll Shop Workshops",
  },
  {
    Department: "SP",
    Installation: "WKSM",
    label: "Mechanical Workshops ",
  },
  {
    Department: "SP",
    Installation: "WKSH",
    label: "Hydraulic Workshops ",
  },
  {
    Department: "SP",
    Installation: "WKST",
    label: "Trainrail Workshops ",
  },
  {
    Department: "SP",
    Installation: "WKSC",
    label: "Electronic control Workshops (eg metrology)",
  },
  {
    Department: "SP",
    Installation: "WKSV",
    label: "Vehicle Workshops/Garage",
  },
  {
    Department: "SP",
    Installation: "WKSE",
    label: "Electrical Workshops",
  },
  {
    Department: "SP",
    Installation: "WKSP",
    label: "Packaging Workshop",
  },
  {
    Department: "SP",
    Installation: "WKSS",
    label: "Storage Workshops",
  },
  {
    Department: "SP",
    Installation: "WKSI",
    label: "Informatics",
  },
  {
    Department: "SP",
    Installation: "WKSU",
    label: "Internal Communication/Telephone",
  },
  {
    Department: "SP",
    Installation: "WKSD",
    label: "Medical/doctor/dentist",
  },
  {
    Department: "SP",
    Installation: "WKSG",
    label: "Gas rescue/prevention",
  },
  {
    Department: "SP",
    Installation: "WKSF",
    label: "Fire prevention ",
  },
  {
    Department: "SP",
    Installation: "WKSL",
    label: "Learning/education center",
  },
  {
    Department: "SP",
    Installation: "WKSK",
    label: "Environment/Pollution/Climate",
  },
  {
    Department: "SP",
    Installation: "WKSN",
    label: "Cranes Workshops",
  },
  {
    Department: "SP",
    Installation: "WKSA",
    label: "AirCo Workshops",
  },
  {
    Department: "SP",
    Installation: "WKSB",
    label: "Electro Mechanical Workshop",
  },
  {
    Department: "SP",
    Installation: "WKSJ",
    label: "Inspection Workshop",
  },
  {
    Department: "SP ",
    Installation: "RLW",
    label: "Railway",
  },
  {
    Department: "SP ",
    Installation: "PIP",
    label: "Pipe Shop",
  },
  {
    Department: "",
    Installation: "",
    label: "",
  },
  {
    Department: "RY",
    Installation: "BRT",
    label: "Transformation to brickets",
  },
  {
    Department: "RY",
    Installation: "BRH",
    label: "Bricketting Hot",
  },
  {
    Department: "RY",
    Installation: "OPS",
    label: "Operations of mettalic obsolescence scrap",
  },
];
