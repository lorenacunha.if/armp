export const departments = [
  {
    label: "Coke Plant",
    abbrev: "CB",
    null: null,
  },
  {
    label: "Sinter Plant & Blast Furnace",
    abbrev: "SB",
    null: null,
  },
  {
    label: "Steel Plant",
    abbrev: "ST",
    null: null,
  },
  {
    label: "Slab, Bloom, Billet Yard",
    abbrev: "SY",
    null: null,
  },
  {
    label: "Hot Rolling Mill",
    abbrev: "HR",
    null: null,
  },
  {
    label: "Long Wiring Mill",
    abbrev: "LW",
    null: null,
  },
  {
    label: "Long Rolling Mill",
    abbrev: "LR",
    null: null,
  },
  {
    label: "Cold Rolling Mill",
    abbrev: "CR",
    null: null,
  },
  {
    label: "Recoiling and Packing",
    abbrev: "RP",
    null: null,
  },
  {
    label: "Coating",
    abbrev: "CO",
    null: null,
  },
  {
    label: "Plate Shop",
    abbrev: "PS",
    null: null,
  },
  {
    label: "Tailor Blanks",
    abbrev: "TB",
    null: null,
  },
  {
    label: "Billets Processing",
    abbrev: "BP",
    null: null,
  },
  {
    label: "Services Production",
    abbrev: "SP",
    null: null,
  },
  {
    label: "Tubular",
    abbrev: "TU",
    null: null,
  },
  {
    label: "Recycling ",
    abbrev: "RY",
    null: null,
  },
  {
    label: "Compact Unit",
    abbrev: "CU",
    null: null,
  },
  {
    label: "Mining Operations",
    abbrev: "MO",
    null: null,
  },
  {
    label: "Direct reduced Iron making",
    abbrev: "DR",
    null: null,
  },
  {
    label: "Crushing Plant ",
    abbrev: "CP",
    null: null,
  },
  {
    label: "Concentrating Plant ",
    abbrev: "CT",
    null: null,
  },
  {
    label: "Shipping",
    abbrev: "SH",
    null: null,
  },
  {
    label: "Warehouse",
    abbrev: "WH",
    null: null,
  },
];
