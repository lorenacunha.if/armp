import { MinusCircleTwoTone, CheckCircleTwoTone } from "@ant-design/icons";
import { Key } from "antd/es/table/interface";
import { DataNode } from "antd/es/tree";
import React, { useState } from "react";

const initialState = {
  currentLevel: 1,
  tree: [] as DataNode[],
  showTree: false,
  nextLevel: () => {},
  initTree: (item: any) => {},
  addToTree: (item: any) => {},
  search: "",
  setSearch: (value: string) => {},
  expandedKeys: [] as Key[],
  setExpandedKeys: (value: React.Key[]) => {},
};

export const TreeContext = React.createContext(initialState);

const TreeStore = ({ children }: any) => {
  const [currentLevel, setCurrentLevel] = useState(1);
  const [showTree, setShowTree] = useState<boolean>(false);
  const [tree, setTree] = useState<DataNode[]>([]);
  const [treeSearch, setTreeSearch] = useState<Key[]>([]);
  const [expandedKeys, setExpandedKeys] = useState<Key[]>([]);
  const [search, setSearch] = useState<string>("");

  const nextLevel = () => {
    console.log({ currentLevel, children });
    setCurrentLevel(currentLevel + 1);
  };

  const initTree = (item: any) => {
    console.log({ item });
    setSearch("");
    setShowTree(true);

    const tree: DataNode = {
      title: item.label,
      key: item.abbrev,
      icon: item.label ? (
        <MinusCircleTwoTone twoToneColor='#D76692' />
      ) : (
        <CheckCircleTwoTone twoToneColor='#45F1C2' />
      ),
      children: [],
    };

    setTree([tree]);
    setExpandedKeys([tree.key]);
    setTreeSearch([tree.key]);
    nextLevel();
  };

  const getChild = (tree: DataNode[], childLabel: Key): DataNode | undefined => {
    return tree.find((item) => item.key === childLabel);
  };

  const getParentKey = (key: React.Key, tree: DataNode[]): React.Key => {
    let parentKey: React.Key;
    for (let i = 0; i < tree.length; i++) {
      const node = tree[i];
      if (node.children) {
        if (node.children.some((item) => item.key === key)) {
          parentKey = node.key;
        } else if (getParentKey(key, node.children)) {
          parentKey = getParentKey(key, node.children);
        }
      }
    }
    return parentKey!;
  };

  const addToTree = (item: any) => {
    const newTree = [...tree];
    let levelItem = newTree;

    const newExpandedKeys: Key[] = [treeSearch[0]];

    expandedKeys.forEach((search, index) => {
      const child = getChild(levelItem, search);
      if (index === expandedKeys.length - 1) {
        const newNode: DataNode = {
          title: item.label,
          key: item.abbrev,
          icon: item.label ? (
            <MinusCircleTwoTone twoToneColor='#D76692' />
          ) : (
            <CheckCircleTwoTone twoToneColor='#45F1C2' />
          ),
          children: [],
        };

        newExpandedKeys.push(item.abbrev);
        child?.children?.push(newNode);
      }
      levelItem = child?.children ? child?.children : levelItem;
    });
    console.log({ newTree, newExpandedKeys });
    setTree(newTree);
    setExpandedKeys(newExpandedKeys);
  };

  return (
    <TreeContext.Provider
      value={{
        currentLevel,
        nextLevel,
        initTree,
        tree,
        showTree,
        addToTree,
        search,
        setSearch,
        expandedKeys,
        setExpandedKeys,
      }}
    >
      {children}
    </TreeContext.Provider>
  );
};

export default TreeStore;
